---
Spike Report
---

Core Spike 7
============

Introduction
------------

Now that the game from Spikes 5 & 6 is complete it's time to deploy it
for others to play. This Spike covers how Unreal Cooks assets and
Packages the game into an executable file.

Bitbucket Link: <https://bitbucket.org/bSalmon842/spike6>

Goals
-----

Building on Core Spikes 5 and 6:

1.  Package your project into an Executable

2.  Document what options you selected, if any, and what effects they
    had on either the Packaging process or the Execution of the game
    (i.e. frame-rate differences, load-time, or packaging-time).

Personnel
---------

  ------------------------- ------------------
  Primary -- Brock Salmon   Secondary -- N/A
  ------------------------- ------------------

Technologies, Tools, and Resources used
---------------------------------------

-   <https://docs.unrealengine.com/latest/INT/Engine/Deployment/>

Tasks undertaken
----------------

1.  Cook the Content for Windows under the 'File' drop-down

2.  Set the Build Configuration, in this case it was the
    Shipping build for Windows 64-bit, however Development
    and Debug 32 and 64 bits were also trialled.

3.  Package the Project for your chosen Platform.

What we found out
-----------------

From this Spike we learned how to Cook the Content of a game so that the
assets can be used on multiple platforms. From there we can now Package
the Project for multiple platforms, including 32-bit and 64-bit
Operating Systems, as well as Zipping the Project to move between
Computers easier. The Build Configuration for this project was around 90
megabytes difference between the Development and Shipping configurations
with very little difference in deployment time, however a larger or more
intensive game could see larger improvements in deployment time and file
size.
